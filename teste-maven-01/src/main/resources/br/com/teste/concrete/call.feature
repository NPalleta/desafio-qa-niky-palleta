Feature: Call to a Contact 
	As a whats app user
	I want do a call to a contact
	Through whatsapp call I can save more money

Scenario: Do a Call 
	Given I have opened a contact chat screen 
	When I touch the telephone icon on the middle of top 
	Then the call should be done 
	
Scenario: Cancel a Call 
	Given I have opened a contact chat screen 
	When I touch the telphone icon on the middle of top 
	And I touch the call cancellation icon 
	Then the call should be canceled