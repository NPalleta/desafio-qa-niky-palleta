Feature: Personalize Contact/Group 
	As a whats app user
	I want to personalize my whats app contacts and groups configuration
	It's possible to know who sent a message before even unlocking the mobile screen

Scenario: Personalize Notification Tone 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the  Notification Tone  option 
	And I select the New Notification Tone 
	And I touch Ok 
	Then the old notification tone should be changed by the new notification tone 
	
Scenario: Personalize Vibration Type 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Vibration Type option 
	And I select the New Vibration Type 
	Then the old vibration type should be changed by the new vibration type 
	
Scenario: Personalize Pop-Up Notification 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Pop-Up Notification option 
	And I select the New Pop-Up Notification 
	Then the old Pop-Up notification should be changed by the new Pop-Up notification 
	
Scenario: Personalize Light 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Light option 
	And I select the New Light 
	Then the old light should be changed by the new light 
	
Scenario: Turn Off Notification Tone 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact 
	And I touch Personalized Notifications 
	And I touch the mark box Personalize 
	And I touch the  Notification Tone 
	And I select Turn Off Notification Tone Option 
	And I touch Ok 
	Then the notification tone should be off now 
	
Scenario: Turn Off Vibration 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Vibration Type option 
	And I select Turn Off vibration option 
	Then the vibration should be off now 
	
Scenario: Turn Off Pop-Up Notification 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Pop-Up Notification option 
	And I select Turn Off New Pop-Up Notification option 
	Then the Pop-Up notification should be off now 
	
Scenario: Turn Off Light 
	Given I have opened a contact chat screen 
	When I touch the 3 dots icon on the right top 
	And I touch Contact option 
	And I touch Personalized Notifications option 
	And I touch the mark box Personalize option 
	And I touch the Light option 
	And I select Turn Off Light option 
	Then the light should be off now 
