package br.com.teste.concrete;

import org.apache.commons.lang.StringUtils;

public class Checkout {

	private String[] items = { "A", "B", "C", "D" };

	private int totalItems, total, scan, discountA, discountB;

	// This is a main method. Responsible for filtering the items and apply the
	// discounts...
	public int getItemPurchasedMainMethod(String itemPurchased) {
		//
		int price = 0;
		int totalItens = 0;
		//
		if (itemPurchased.contains(items[0])) {

			totalItens = getItemPurchasedFirstMethod(itemPurchased, items[0]);
			for (int countItem = 1; countItem <= totalItens; countItem++) {
				if (countItem % 3 == 0) {
					price += 30;
				} else {
					price += 50;
				}
			}
		}
		//
		if (itemPurchased.contains(items[1])) {
			totalItens = getItemPurchasedFirstMethod(itemPurchased, items[1]);
			for (int countItem = 1; countItem <= totalItens; countItem++) {
				if (countItem % 2 == 0) {
					price += 15;
				} else {
					price += 30;
				}
			}
		}
		//
		if (itemPurchased.contains(items[2])) {
			totalItens = getItemPurchasedSecondMethod(itemPurchased, items[2]);
			totalItens *= 20;
			price += totalItens;
		}
		//
		if (itemPurchased.contains(items[3])) {
			totalItens = getItemPurchasedThirdMethod(itemPurchased, items[3]);
			totalItens *= 15;
			price += totalItens;
		}
		//
		System.out.println(price);
		//
		return price;
	}

	// Method to separate the items ... Here using a character comparison...
	public int getItemPurchasedFirstMethod(String itemPurchased, String item) {
		//
		int countChar = 0;
		//
		char itemToChar = item.charAt(0);
		//
		for (int i = 0; i < itemPurchased.length(); i++) {
			if (itemPurchased.charAt(i) == itemToChar) {
				countChar++;
			}
		}
		return countChar;
	}

	// The same as above but using the replaceAll function...
	public int getItemPurchasedSecondMethod(String itemPurchased, String item) {
		totalItems = itemPurchased.replaceAll("[^" + item + "]", "").length();
		return totalItems;
	}

	// Using countMatches function from Apache Commons Lang Library...
	public int getItemPurchasedThirdMethod(String itemPurchased, String item) {
		totalItems = StringUtils.countMatches(itemPurchased, item);
		return totalItems;
	}

	public void setScan(int scan) {
		this.scan = scan;
	}

	// Calculates the discounts...
	public void getScan(String item) {

		if (items[0] == item) {
			discountA++;
			if (discountA % 3 == 0) {
				scan = 30;
			} else {
				scan = 50;
			}
		}
		//
		if (items[1] == item) {
			discountB++;
			if (discountB % 2 == 0) {
				scan = 15;
			} else {
				scan = 30;
			}
		}
		//
		if (items[2] == item) {
			scan = 20;
		}
		//
		if (items[3] == item) {
			scan = 15;
		}
		//
	}

	public void setTotal(int total) {
		this.total = total;
	}

	// Calculate the total, adding the prices...
	public int getTotal() {
		total += scan;
		return total;
	}
}