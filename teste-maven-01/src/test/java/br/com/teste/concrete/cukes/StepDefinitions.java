package br.com.teste.concrete.cukes;

import static org.junit.Assert.assertEquals;

import br.com.teste.concrete.Checkout;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {

	public int price;

	@Given("^I put in my cart this sequence of items: (.*)$")
	public void i_put_in_my_cart_this_sequence_of_items(String items) throws Throwable {
		Checkout checkout = new Checkout();
		price = checkout.getItemPurchasedMainMethod(items);
	}

	@When("^(\\d+) dollars cost everything$")
	public void dollars_cost_everything(int price) throws Throwable {
	}

	@Then("^The discount should give the total of (\\d+)$")
	public void the_discount_should_give_the_total_of(int price) throws Throwable {
		assertEquals(this.price, price);
	}
}
