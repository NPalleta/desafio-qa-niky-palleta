package br.com.teste.junit.concrete;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import br.com.teste.concrete.Checkout;

public class CheckoutJunitTest {

	@Test
	public void testTotals() {

		Checkout checkout = new Checkout();

		assertEquals(0, checkout.getItemPurchasedMainMethod(""));
		assertEquals(50, checkout.getItemPurchasedMainMethod("A"));
		assertEquals(80, checkout.getItemPurchasedMainMethod("AB"));
		assertEquals(115, checkout.getItemPurchasedMainMethod("CDBA"));
		//
		assertEquals(100, checkout.getItemPurchasedMainMethod("AA"));
		assertEquals(130, checkout.getItemPurchasedMainMethod("AAA"));
		assertEquals(180, checkout.getItemPurchasedMainMethod("AAAA"));
		assertEquals(230, checkout.getItemPurchasedMainMethod("AAAAA"));
		assertEquals(260, checkout.getItemPurchasedMainMethod("AAAAAA"));
		//
		assertEquals(160, checkout.getItemPurchasedMainMethod("AAAB"));
		assertEquals(175, checkout.getItemPurchasedMainMethod("AAABB"));
		assertEquals(190, checkout.getItemPurchasedMainMethod("AAABBD"));
		assertEquals(190, checkout.getItemPurchasedMainMethod("DABABA"));
	}

	@Test
	public void testIncremental() {
		Checkout checkout = new Checkout();

		assertEquals(0, checkout.getTotal());
		checkout.getScan("A");
		assertEquals(50, checkout.getTotal());
		checkout.getScan("B");
		assertEquals(80, checkout.getTotal());
		checkout.getScan("A");
		assertEquals(130, checkout.getTotal());
		checkout.getScan("A");
		assertEquals(160, checkout.getTotal());
		checkout.getScan("B");
		assertEquals(175, checkout.getTotal());
	}

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(CheckoutJunitTest.class);
		//
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		//
		System.out.println(result.wasSuccessful());
	}
}
